import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Tenant } from '../models/tenant';

@Injectable({
  providedIn: 'root'
})
export class TenantInfoService {
  constructor(
    private store: Store<{ Tenants: Tenant[] }>) {
  }

  getAllTenants() {
    var res: Tenant[] = [];
    res = this.store.pipe(select('Tenants'))["actionsObserver"]["_value"]["payload"];
    return res;
  }
}
