import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { SessionManagerService } from '../session-manager.service';

@Injectable({
  providedIn: 'root'
})
export class CanActivateGuardService implements CanActivate {
  canActivate() {
    return this.sessionManager.isLoggedInUser();
  }

  constructor(private sessionManager: SessionManagerService) { }
}
