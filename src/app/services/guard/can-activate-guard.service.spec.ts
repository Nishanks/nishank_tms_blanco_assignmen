import { TestBed } from '@angular/core/testing';

import { CanActivateGuardService } from './can-activate-guard.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

describe('CanActivateGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule,RouterModule.forRoot([])]

  }));

  it('should be created', () => {
    const service: CanActivateGuardService = TestBed.get(CanActivateGuardService);
    expect(service).toBeTruthy();
  });
});
