import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionManagerService {

  constructor(private route: ActivatedRoute) { }

  setActiveUser(user) {
    sessionStorage.setItem("currentUser", JSON.stringify(user))
  }

  getActiveUser() {
    return JSON.parse(sessionStorage.getItem("currentUser"));
  }

  isLoggedInUser() {
    let currentUser = this.getActiveUser();
    if (currentUser)
      return this.route.snapshot.paramMap.get("id") == this.getActiveUser()["Id"];
    return false;
  }
}
