import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { RestApiService } from './rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  currentTheme: string="default";
  constructor(@Inject(DOCUMENT) private document,private restApiService:RestApiService) { }

  applyTheme(theme) {
    this.currentTheme = theme?theme:this.currentTheme ;
    if (this.currentTheme.toLocaleUpperCase() == 'Black'.toLocaleUpperCase()) {
      this.document.getElementById('theme').href = '../assets/bootstrap.bw.min.css';
      this.currentTheme = 'black';
    }
    else if (this.currentTheme.toLocaleUpperCase() == 'green'.toLocaleUpperCase()) {
      this.document.getElementById('theme').href = '../assets/bootstrap.gw.min.css';
      this.currentTheme = 'green';
    }
    else {
      this.document.getElementById('theme').href = '../assets/bootstrap.default.min.css';
      this.currentTheme = 'default';
    }
  }
}
