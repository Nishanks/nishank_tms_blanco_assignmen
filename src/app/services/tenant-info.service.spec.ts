import { TestBed } from '@angular/core/testing';

import { TenantInfoService } from './tenant-info.service';
import { select, Store, StoreModule } from '@ngrx/store';
import { Tenant } from '../models/tenant';
import { TenantReducer } from '../datahub/tenant.reducer';
describe('TenantInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [StoreModule.forRoot({ tenants: TenantReducer })]
  }));

  it('should be created', () => {
    const service: TenantInfoService = TestBed.get(TenantInfoService);
    expect(service).toBeTruthy();
  });
});
