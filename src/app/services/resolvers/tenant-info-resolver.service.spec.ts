import { TestBed } from '@angular/core/testing';

import { TenantInfoResolverService } from './tenant-info-resolver.service';
import { HttpClientModule } from '@angular/common/http';

describe('TenantInfoResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: TenantInfoResolverService = TestBed.get(TenantInfoResolverService);
    expect(service).toBeTruthy();
  });
});
