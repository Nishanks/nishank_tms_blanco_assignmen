import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { RestApiService } from '../rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class TenantInfoResolverService implements Resolve<any> {
  constructor(private restService: RestApiService) { }
  resolve(): Observable<any> {
    return this.restService.get('assets/mockData.json');
  }

}
