import { TestBed } from '@angular/core/testing';

import { SessionManagerService } from './session-manager.service';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('SessionManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule,RouterModule.forRoot([])],
    providers:[]
  }));

  it('should be created', () => {
    const service: SessionManagerService = TestBed.get(SessionManagerService);
    expect(service).toBeTruthy();
  });
});
