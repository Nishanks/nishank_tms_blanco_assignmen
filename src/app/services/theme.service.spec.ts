import { TestBed } from '@angular/core/testing';

import { ThemeService } from './theme.service';
import { HttpClientModule } from '@angular/common/http';

describe('ThemeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule ]


  }));

  it('should be created', () => {
    const service: ThemeService = TestBed.get(ThemeService);
    expect(service).toBeTruthy();
  });
});
