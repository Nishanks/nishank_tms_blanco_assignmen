import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from '@ngrx/store';
import * as $ from 'jquery';
import { TenantUpdate } from 'src/app/datahub/Tenant.actions';
import { Tenant } from 'src/app/models/tenant';
import { SessionManagerService } from 'src/app/services/session-manager.service';
import { TenantInfoService } from 'src/app/services/tenant-info.service';
import { ThemeService } from 'src/app/services/theme.service';
@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {
  tenantInfo: Tenant;
  currentTenantId: string;

  constructor(
    private route: ActivatedRoute,
    private sessionManager: SessionManagerService,
    private router: Router,
    private store: Store<{ Tenants: Tenant[] }>,
    private tenantInfoService: TenantInfoService,
    private themeService: ThemeService) {

    this.store.dispatch(new TenantUpdate(this.route.snapshot.data['tenants']));
    this.currentTenantId = this.route.snapshot.paramMap.get("id");

    this.tenantInfo = this.sessionManager.getActiveUser();
    this.tenantInfo = this.tenantInfo ? this.tenantInfo : this.tenantInfoService.getAllTenants().find(x => x.Id == this.currentTenantId);
    if (!this.tenantInfo) {
      this.router.navigate(['login']);
    }
    this.themeService.applyTheme(this.tenantInfo.MainColor);

  }
  data: any = { Account: {}, Credentials: {}, PersonalDetails: {}, AddressDetails: {} };
  ngOnInit() {
    var curEl = this.tenantInfo;
    this._init(curEl);
  }
  _init(el: Tenant) {
    $(document).ready(function () {
      var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

      allWells.hide();

      navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
          $item = $(this);

        if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
        }
      });

      allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;
        if (curStepBtn == "step-5" && el.Configuration.CustomConfirmationPage == 1) {
          window.location.href = el.Configuration.Url;
        }
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
          if (!curInputs[i]["validity"].valid) {
            isValid = false;
            $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
      });

      $('div.setup-panel div a.btn-primary').trigger('click');
    });
  }

  Save() {
    console.log(this.data);
  }
}
