import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { TenantUpdate } from 'src/app/datahub/Tenant.actions';
import { Tenant } from 'src/app/models/tenant';
import { RestApiService } from 'src/app/services/rest-api.service';
import { SessionManagerService } from 'src/app/services/session-manager.service';
import { TenantInfoService } from 'src/app/services/tenant-info.service';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ["login.component.scss"]
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    error: boolean;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private tenantInfoService: TenantInfoService,
        private restApiService: RestApiService,
        private sessionManager: SessionManagerService,
        private store: Store<{ Tenants: Tenant[] }>
    ) {
        let data: Tenant[] = this.route.snapshot.data['tenants'];
        this.store.dispatch(new TenantUpdate(data));
    }

    ngOnInit() {

    }

    login() {
        this.loading = true;
        this.error = false;
        this.sessionManager.setActiveUser(null);
        this.store.pipe(select('Tenants')).subscribe(_=>console.log(_));
        this.checkLoginInfo(this.tenantInfoService.getAllTenants());
    }

    checkLoginInfo(res) {
        let item: Tenant = res.find(x => x.UserName == this.model.username);
        if (!item) {
            console.log("Username not available");
            this.error = true;
            this.loading = false;
        }
        else {
            if (item.Password == this.model.password) {
                this.sessionManager.setActiveUser(item);
                this.router.navigate(['tenantInfo', item.Id]);
            }
            else {
                console.log("Password incorrect");
                this.sessionManager.setActiveUser(null);
                this.error = true
            };
            this.loading = false;
        }
    }
}
