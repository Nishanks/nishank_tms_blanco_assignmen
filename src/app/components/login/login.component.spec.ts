import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Routes, RouterModule } from '@angular/router';
import { select, Store, StoreModule } from '@ngrx/store';
import { TenantUpdate } from 'src/app/datahub/Tenant.actions';
import { Tenant } from 'src/app/models/tenant';
import { RestApiService } from 'src/app/services/rest-api.service';
import { SessionManagerService } from 'src/app/services/session-manager.service';
import { TenantInfoService } from 'src/app/services/tenant-info.service';
import { LoginComponent } from './login.component';
import { TenantInfoResolverService } from 'src/app/services/resolvers/tenant-info-resolver.service';
import { TenantInfoComponent } from '../tenant-info/tenant-info.component';
import { OnboardingComponent } from '../onboarding/onboarding.component';
import { AppComponent } from 'src/app/app.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TenantReducer } from 'src/app/datahub/tenant.reducer';
import { CanActivateGuardService } from 'src/app/services/guard/can-activate-guard.service';
const appRoutes: Routes = [
  {
    path: 'login', component: LoginComponent, resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: 'tenantInfo/:id',
    component: TenantInfoComponent,
    resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: 'onboarding/:id', component: OnboardingComponent,
    resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LoginComponent,
        TenantInfoComponent,
        OnboardingComponent
      ],
      imports: [
        RouterModule.forRoot(
          appRoutes,
          { enableTracing: false }
        ),
        BrowserModule,
        AppRoutingModule,
        RouterModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ tenants: TenantReducer })
      ],
      providers: [CanActivateGuardService, TenantInfoResolverService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
