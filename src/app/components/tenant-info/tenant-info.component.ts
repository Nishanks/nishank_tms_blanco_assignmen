import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TenantUpdate } from 'src/app/datahub/Tenant.actions';
import { Tenant } from 'src/app/models/tenant';
import { SessionManagerService } from 'src/app/services/session-manager.service';
import { TenantInfoService } from 'src/app/services/tenant-info.service';
import { ThemeService } from 'src/app/services/theme.service';
@Component({
  selector: 'app-tenant-info',
  templateUrl: './tenant-info.component.html',
  styleUrls: ['./tenant-info.component.scss']
})
export class TenantInfoComponent implements OnInit {

  tenantId: string;
  tenantInfo: Tenant = new Tenant();
  showUrl: boolean = false;
  Tenants: Observable<Tenant[]>;
  submitted: boolean;
  loading: boolean;
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private tenantInfoService: TenantInfoService,
    private cdr: ChangeDetectorRef,
    private store: Store<{ Tenants: Tenant[] }>,
    private sessionManager: SessionManagerService,
    private router: Router,
    private themeService: ThemeService
  ) {

  }

  ngOnInit() {
    let data: Tenant[] = this.route.snapshot.data['tenants'];
    this.store.dispatch(new TenantUpdate(data));
    if (this.sessionManager.getActiveUser()["Id"] != this.route.snapshot.paramMap.get('id')) {
      this.sessionManager.setActiveUser(null);
      this.router.navigate(['login']);
    }
    this.init();
  }
  init() {
    this.tenantId = this.route.snapshot.paramMap.get('id');
    this.tenantInfo = this.sessionManager.getActiveUser() ? this.sessionManager.getActiveUser() : this.tenantInfo;
    this.themeService.applyTheme(this.tenantInfo.MainColor);
  }
  save() {
    this.submitted = false;
    this.loading = true;
    let allTenants: Tenant[] = this.tenantInfoService.getAllTenants();
    allTenants[allTenants.findIndex(x => x.Id == this.tenantInfo.Id)] = this.tenantInfo;
    this.sessionManager.setActiveUser(this.tenantInfo);
    this.store.dispatch(new TenantUpdate(allTenants));
    this.themeService.applyTheme(this.tenantInfo.MainColor);
    this.submitted = true;
    this.loading = false;
  }

  goToOnboarding() {
    this.router.navigate(['onboarding', this.tenantInfo.Id]);
  }
}
