import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { RestApiService } from './services/rest-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  currentTheme: string="black";

  constructor(@Inject(DOCUMENT) private document,private restApiService:RestApiService) {
  }

  ngOnInit() {
    this.currentTheme = 'default';
    this.document.getElementById('theme').href = '../assets/bootstrap.default.min.css';
  }

  handelChangeTheme(event) {
    this.currentTheme = event;
    if (this.currentTheme == 'black') {
      this.document.getElementById('theme').href = '../assets/bootstrap.bw.min.css';
      this.currentTheme = 'black';
    }
    else if (this.currentTheme == 'default') {
      this.document.getElementById('theme').href = '../assets/bootstrap.default.min.css';
      this.currentTheme = 'default';
    }
    else if (this.currentTheme == 'green') {
      this.document.getElementById('theme').href = '../assets/bootstrap.gw.min.css';
      this.currentTheme = 'green';
    }
  }
}