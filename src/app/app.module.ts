import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TenantInfoComponent } from './components/tenant-info/tenant-info.component';
import { OnboardingComponent } from './components/onboarding/onboarding.component';

import { StoreModule } from '@ngrx/store';
import { TenantReducer } from './datahub/tenant.reducer';
import { CanActivateGuardService } from './services/guard/can-activate-guard.service';
import { TenantInfoResolverService } from './services/resolvers/tenant-info-resolver.service';

const appRoutes: Routes = [
  {
    path: 'login', component: LoginComponent, resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: 'tenantInfo/:id',
    component: TenantInfoComponent,
    resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: 'onboarding/:id', component: OnboardingComponent,
    resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TenantInfoComponent,
    OnboardingComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    ),
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ tenants: TenantReducer })
  ],
  providers: [CanActivateGuardService, TenantInfoResolverService],
  bootstrap: [AppComponent]

})
export class AppModule { }
