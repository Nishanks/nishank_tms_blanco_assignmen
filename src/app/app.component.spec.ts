import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { RestApiService } from './services/rest-api.service';
import { LoginComponent } from './components/login/login.component';
import { TenantInfoComponent } from './components/tenant-info/tenant-info.component';
import { OnboardingComponent } from './components/onboarding/onboarding.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { TenantReducer } from './datahub/tenant.reducer';
import { CanActivateGuardService } from './services/guard/can-activate-guard.service';
import { TenantInfoResolverService } from './services/resolvers/tenant-info-resolver.service';

const appRoutes: Routes = [
  {
    path: 'login', component: LoginComponent, resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: 'tenantInfo/:id',
    component: TenantInfoComponent,
    resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: 'onboarding/:id', component: OnboardingComponent,
    resolve: {
      tenants: TenantInfoResolverService
    }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LoginComponent,
        TenantInfoComponent,
        OnboardingComponent
      ],
      imports: [
        RouterModule.forRoot(
          appRoutes,
          { enableTracing: false }
        ),
        BrowserModule,
        AppRoutingModule,
        RouterModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ tenants: TenantReducer })
      ],
      providers: [CanActivateGuardService, TenantInfoResolverService]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
