export class Configuration {
    AllowedAccountTypes:AllowedAccountTypes;
    AskAddressDetails: any;
    AskCredentials: any;
    CustomConfirmationPage: any;
    Url: string;
}

export class Tenant {
    readonly Id: string;
    Name: string;
    MainColor: string;
    SecondaryColor: string;
    Configuration: Configuration;
    readonly UserName: string;
    readonly Password: string;
}

export interface AllowedAccountTypes {
    All: boolean;
    Individual: boolean;
    Business: boolean;
}