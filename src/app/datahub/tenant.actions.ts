import { Action } from '@ngrx/store';
import { Tenant } from '../models/tenant';
export enum TenantActionTypes {
    Update = '[Tenant Component] Update'
}
export class ActionEx implements Action {
    readonly type;
    payload: Tenant[];
}
export class TenantUpdate implements ActionEx {
    readonly type = TenantActionTypes.Update;
    constructor(public payload: Array<Tenant>) {
    }
}
