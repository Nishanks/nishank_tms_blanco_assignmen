
import { Tenant } from '../models/tenant';
import { ActionEx, TenantActionTypes } from './Tenant.actions';
export const initialState = { Tenants: Array<Tenant>()};
export function TenantReducer(state = initialState, action: ActionEx) {
  switch (action.type) {
    case TenantActionTypes.Update:
      return { Tenants: action.payload }
    default:
      return state;
  }
}